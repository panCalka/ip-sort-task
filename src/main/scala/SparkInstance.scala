import org.apache.spark.sql.SparkSession

trait SparkInstance {
  val spark = SparkSession.builder
    .master("local")
    .appName("common-context")
    .getOrCreate()
}
