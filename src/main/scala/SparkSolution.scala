import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.functions.collect_list

object SparkSolution extends SparkInstance{
  import spark.implicits._

  def ipGroup: String => String = {
    _.split('.').dropRight(1).mkString(".")
  }

  import org.apache.spark.sql.functions.udf
  val ipUDF = udf(ipGroup)

  def groupByIp(input: Dataset[String]) =
    input
      .withColumn("ip-ranges", ipUDF('value))
      .groupBy($"ip-ranges")
      .agg(collect_list($"value").as("values"))

}
