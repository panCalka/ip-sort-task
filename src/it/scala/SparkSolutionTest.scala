import org.scalatest.FlatSpec
import org.scalatest.Matchers.convertToAnyShouldWrapper

class SparkSolutionTest extends FlatSpec with SparkTestInstance {
  import spark.implicits._

  it should "assign ips to given range" in {
    //given
    val input = Seq("145.67.101.31", "145.67.101.32", "145.67.101.33", "144.67.101.32", "144.67.101.33").toDS

    //when
    val result = SparkSolution.groupByIp(input)

    //TODO: exact dataframe structure should not be used here (use contains sameElementsAs or so)
    val expected = Seq(
      ("144.67.101", Seq("144.67.101.32", "144.67.101.33")),
      ("145.67.101", Seq("145.67.101.31", "145.67.101.32", "145.67.101.33"))).toDF("ip-ranges", "values")

    //then
   result.except(expected).collect() shouldEqual Seq.empty

  }
}
