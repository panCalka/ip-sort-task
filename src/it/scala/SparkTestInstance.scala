import org.apache.spark.sql.SparkSession

trait SparkTestInstance {
  val spark = SparkSession.builder
    .master("local")
    .appName("common-test-context")
    .getOrCreate()

  spark.sparkContext.setLogLevel("ERROR")
}
